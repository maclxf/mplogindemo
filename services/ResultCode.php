<?php
namespace services;

Class ResultCode
{
    protected static $commonMappings = [
        'SUCCESS' => [
            'code' => 20000,
            'message' => '成功',
        ],
        'FAIL' => [
            'code' => 40000,
            'message' => '失败',
        ],
        'UNKNOW' => [
            'code' => 99999,
            'message' => '未知错误',
        ],
        // 登录相关
        'ERROR_NAME_PASSWORD' => [
            'code' => 40001,
            'message' => '用户名或者密码错误',
        ],
        'EMPTY_NAME' => [
            'code' => 40002,
            'message' => '用户名不能为空',
        ],
        'EMPTY_PASSWORD' => [
            'code' => 40003,
            'message' => '密码不能为空',
        ],
        //教师信息
        'NO_TOKEN' => [
            'code' => 40004,
            'message' => '未提供TOKEN',
        ]
    ];

    public static function getResult(string $key)
    {
        if (array_key_exists($key, self::$commonMappings)) {
            return self::$commonMappings[$key];
        }

        return self::$commonMappings['UNKNOW'];
    }
}
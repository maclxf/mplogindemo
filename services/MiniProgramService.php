<?php


namespace services;

use EasyWeChat\Factory as MiniProgramFactory;
use think\facade\Env;

class MiniProgramService
{

    private static function createApp()
    {
        // TODO::配置需要转移到环境变量
        $config = [
            'app_id' => Env::get('wechat.app_id', ''),
            'secret' => Env::get('wechat.secret', ''),

            // 下面为可选项
            // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
            'response_type' => 'array',

            'log' => [
                'level' => 'debug',
            //    'file' => __DIR__.'/wechat.log',
            ],
        ];

        return MiniProgramFactory::miniProgram($config);

    }

    public static function login(string $code)
    {
        $app = self::createApp();
        return $app->auth->session($code);

    }

    public static function encryptor(string $sessionKey, string $iv, string $encryptData)
    {
        $app = self::createApp();
        return $app->encryptor->decryptData($sessionKey, $iv, $encryptData);
    }
}
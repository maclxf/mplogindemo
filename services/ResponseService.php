<?php
namespace services;

use services\ResultCode;

class ResponseService
{
    private static $success = 'success';
    private static $fail = 'fail';


    private static $status;
    private static $code;
    private static $message;
    private static $data;

    public static function success(array $data = [])
    {
        self::setResult(self::$success, 'SUCCESS');
        // if ($data) {
            self::setData($data);
        // }
        return self::class;
    }

    public static function fail(array $data = [])
    {
        self::setResult(self::$fail, 'FAIL');
        // if ($data) {
            self::setData($data);
        // }
        return self::class;
    }

    public static function result($status, $codeKey, $data)
    {
        self::setResult($status, $codeKey);
        // if ($data) {
            self::setData($data);
        // }
        return self::class;
    }

    public static function sendResult()
    {
        return json([
            'status' => self::$status,
            'code' => self::$code,
            'message' => self::$message,
            'data' => self::$data
        ]);
    }

    private static function setResult(string $status, string $codeKey, $data = [])
    {
        $result = ResultCode::getResult($codeKey);

        self::$status = $result['code'] === 99999 ? self::$fail : $status;
        self::$code = $result['code'];
        self::$message = $result['message'];
    }

    private static function setData(array $data)
    {
        self::$data = $data;
    }



    // private static function sendResult($status, string $codeKey, array $data)
    // {
    //     $result = ResultCode::getResult($key);

    //     return json([
    //         'status' => $result['code'] === 40000 ? self::$fail : $status,
    //         'code' => $result['code'],
    //         'message' => $result['message'],
    //         'data' => $data
    //     ]);
    // }
}
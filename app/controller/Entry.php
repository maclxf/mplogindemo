<?php
declare (strict_types = 1);

namespace app\controller;

use app\BaseController;
use app\Request;
use services\MiniProgramService;
use services\ResponseService;
use think\cache\driver\Redis;


class Entry extends BaseController
{
    public function wechatLogin(Request $request)
    {
        $code = $request->param('code');
        $ret = MiniProgramService::login($code);


        $session_key = $ret['session_key'] ?? '';
        $openid = $ret['openid'] ?? '';
        $unionid = $ret['unionid'] ?? '';

        $authToken = md5($openid . time());
        $uid = 1;
        $busiIdentity = 'VISIT';
        $nickName = '';
        $headUrl = '';
        $phone = '';

        $redis = new Redis();
        $redis->hmset('MPLOGINDEMO:' . $authToken, compact(
            'uid',
            'busiIdentity',
            'nickName',
            'headUrl',
            'phone',
            'session_key',
            'openid'
        ));

        $userInfo = compact(
            'uid',
            'busiIdentity',
            'nickName',
            'headUrl',
            'phone',
            'session_key',
            'openid'
        );
        return ResponseService::success([
            'authToken' => $authToken,
            'userInfo' => $userInfo
        ])::sendResult();

    }

    public function encrypetdPhone(Request $request) {
        $iv = $request->param('iv');
        $encryptedData = $request->param('encryptedData');
        $session_key = $request->session_key();

        $phone = MiniProgramService::encryptor($session_key, $iv, $encryptedData);

        $authToken = $request->authToken();
        $redis = new Redis();

        $redis->hmset('MPLOGINDEMO:' . $authToken, [
            'phone' => $phone['phoneNumber'] ?? '',
            'nickName' => 'u_' . $phone['phoneNumber'],
            'busiIdentity' => 'MEMBER'
        ]);

        $userInfo = $redis->hgetall('MPLOGINDEMO:' . $authToken);

        return ResponseService::success([
            'authToken' => $authToken,
            'userInfo' => $userInfo
        ])::sendResult();
    }

    public function encrypetdUserInfo(Request $request)
    {
        $iv = $request->param('iv');
        $encryptedData = $request->param('encryptedData');
        $session_key = $request->session_key();

        $wechatCustomer = MiniProgramService::encryptor($session_key, $iv, $encryptedData);

        $authToken = $request->authToken();
        $redis = new Redis();

        $redis->hmset('MPLOGINDEMO:' . $authToken, [
            //'phone' => $phone['phoneNumber'] ?? '',
            'nickName' => $wechatCustomer['nickName'],
            'busiIdentity' => 'MEMBER',
            'headUrl' => $wechatCustomer['avatarUrl']
        ]);

        $userInfo = $redis->hgetall('MPLOGINDEMO:' . $authToken);

        return ResponseService::success([
            'authToken' => $authToken,
            'userInfo' => $userInfo
        ])::sendResult();
    }

    public function notice()
    {
        return ResponseService::success([
            'notices' => [
                [
                    'date' => '2021-05-04',
                    'comment' => '两江VS沧州不败'
                ],
                [
                    'date' => '2021-05-04',
                    'comment' => '广州城VS青岛领先'
                ]
            ],
        ])::sendResult();
    }

    public function order()
    {
        return ResponseService::success([
            'orders' => [
                [
                    'date' => '2021-05-04',
                    'tid' => 'AT123346',
                    'receiver' => '老三'
                ],
                [
                    'date' => '2021-05-02',
                    'tid' => 'AT7891111',
                    'receiver' => '老大'
                ]
            ]
        ])::sendResult();
    }
}

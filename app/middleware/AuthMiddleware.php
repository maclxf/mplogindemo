<?php
declare (strict_types = 1);

namespace app\middleware;

use services\ResponseService;
use think\cache\driver\Redis;
use think\Request;

class AuthMiddleware
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle(Request $request, \Closure $next)
    {
        $token = $request->header('auth');
        if (!$token) {
            return ResponseService::result('fail', 'NO_TOKEN', [])::sendResult();
        }
        $authToken = str_replace('Bearer ', '', $token);

        $redis = new Redis();
        $data = $redis->hgetall('MPLOGINDEMO:' . $authToken);


        $request::macro('session_key', function() use($data) {
            return $data['session_key'];
        });

        $request::macro('authToken', function() use($authToken) {
            return $authToken;
        });

        return $next($request);
    }
}

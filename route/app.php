<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use app\middleware\AuthMiddleware;
use think\facade\Route;

Route::group('v1', function() {
    Route::post('wechatLogin', 'entry/wechatLogin');


    Route::get('notice', 'entry/notice');

    Route::group('auth', function() {
        Route::get('order', 'entry/order');
        Route::post('encrypetdPhone', 'entry/encrypetdPhone');
        Route::post('encrypetdUserInfo', 'entry/encrypetdUserInfo');


    })->middleware(AuthMiddleware::class);


});


//Route::get('hello/:name', 'index/hello');
